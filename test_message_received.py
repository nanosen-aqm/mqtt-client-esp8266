import paho.mqtt.client as mqtt
from validate_message import validate_message

secret_key = "nanosen12345678"
# mqtt_broker = "nano1.xdi.uevora.pt"
# mqtt_broker = "localhost"
#mqtt_broker = "iot.eclipse.org"
mqtt_broker = "broker.hivemq.com"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/mjson")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
	
	print "Message received"
	print(msg.payload)

	message_validation = validate_message(msg.payload, secret_key)
	print message_validation

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

#client.connect("iot.eclipse.org", 1883, 60)
client.connect(mqtt_broker, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()