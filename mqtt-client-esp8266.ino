#include <ESP8266WiFi.h>
#include <DHT.h>;
#include <WiFiUdp.h>
#include <Time.h>

// WiFi configuration file
#include "config.h"

// https://github.com/256dpi/arduino-mqtt
#include <MQTT.h>

// https://github.com/tzikis/ArduinoMD5
#include <MD5.h>

WiFiClient wifiClient;

// The maximum size for packets being published is set by default
// to 128 bytes. This value should be larger than the size of the
// message to be published
MQTTClient mqttClient(320);

const char* ssid     = WIFI_SSID;
const char* password = WIFI_PASSWORD;

//const char* mqttServer = "iot.eclipse.org";
//const char* mqttServer = "broker.hivemq.com";
const char* mqttServer = "nano1.xdi.uevora.pt";
//const char* mqttServer = "192.168.1.72";
const char* mqttUsername = "username";
const char* mqttPassword = "password";
char privateKey[16] = "nanosen12345678";

float temperature;
float humidity;

#define DHTPIN D7     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino


//*-------- NTP code ----------*/
unsigned int localPort = 51349;          // local port to listen for UDP packets

IPAddress timeServer(5, 135, 59, 152);  // ntp1.unixcraft.org. NTP server

const int timeZone = 1;                 // Central European Time

const int NTP_PACKET_SIZE = 48;         // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE];    //buffer to hold incoming and outgoing packets

WiFiUDP Udp;                        // A UDP instance to let us send and receive packets over UDP


void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  dht.begin();

  mqttClient.begin(mqttServer, wifiClient);
  mqttConnect();

  Udp.begin(localPort);
  setSyncProvider(getNtpTime);
}

void mqttConnect() {
  while (!mqttClient.connected()) {

    Serial.print("Attempting MQTT connection...");

    if (mqttClient.connect("myClientID", mqttUsername, mqttPassword)) {
      Serial.println("connected");
    }
    else {
      Serial.print("failed, rc=");
      //Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");

      // Wait 2 seconds before retrying
      delay(2000);
    }
  }
}


void loop() {

  if (!mqttClient.connected()) {
    mqttConnect();
  }

  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  Serial.print("Temperature: ");
  Serial.println(temperature);

  Serial.print("Humidity: ");
  Serial.println(humidity);
  
  publishMessage("3390", temperature);
  publishMessage("3389", humidity);

  delay(2000);
}

void publishMessage(char sensorId[], float value) {
  char * date =  getDate();
  Serial.println(date);

  char sensorValue[16];
  Serial.print("value");
  Serial.println(value);
  
  dtostrf(value, 4, 2, sensorValue);

  Serial.print("sensorValue");
  Serial.println(sensorValue);
  
  // build json message
  char message[256] = "";
  strcat(message, "{\"device_id\":\"");
  strcat(message, "123456789");

  strcat(message, "\",\"message_date\":\"");
  strcat(message, date);

  strcat(message, "\",\"latitude\":\"");
  strcat(message, "-38.5714");

  strcat(message, "\",\"longitude\":\"");
  strcat(message, "-7.9135");

  strcat(message, "\",\"sensors\":[{\"sensor_id\":\"");
  strcat(message, sensorId);
  strcat(message, "\",\"data\":[{\"date\":\"");

  strcat(message, date);
  free(date);

  strcat(message, "\",\"value\":\"");
  strcat(message, sensorValue);
  strcat(message, "\"");
  strcat(message, "}]}]}");

  //save message size to remove privateKey after message hash is done
  int messageSize = strlen(message);

  //append private key to message
  strcat(message, privateKey);

  //sign message together with private key
  unsigned char* md5_hash = MD5::make_hash(message);
  char *md5_hash_str = MD5::make_digest(md5_hash, 16);
  Serial.println(md5_hash_str);
  free(md5_hash);

  //mark the end of the message at before the private key
  //(remove the private key)
  message[messageSize] = '\0';

  // build final message to be published by appending
  // the "signature" with the original message
  char signed_message[256] = "";
  strcat(signed_message, "{\"message\":");
  strcat(signed_message, message);
  strcat(signed_message, ", \"authentication\":\"");
  strcat(signed_message, md5_hash_str);
  strcat(signed_message, "\"");
  strcat(signed_message, "}");
  free(md5_hash_str);

  Serial.println("Message: ");
  Serial.println(signed_message);
  Serial.print("\nMessage size: ");
  Serial.println(strlen(signed_message));

  boolean rc = mqttClient.publish("/mjson", signed_message);
}

/*-------- NTP code ----------*/
time_t getNtpTime()
{
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

char * getDate() {
  char *dateString = (char *) malloc(sizeof(char) * 32);
  strcpy (dateString, "");

  char dateBuf[4] = "";

  dtostrf(year(), 4, 0, dateBuf);
  strcat(dateString, dateBuf);
  strcat(dateString, "-");

  if (month() < 10) {
    strcat(dateString, "0");
    dtostrf(month(), 1, 0, dateBuf);
  } else {
    dtostrf(month(), 2, 0, dateBuf);
  }
  strcat(dateString, dateBuf);
  strcat(dateString, "-");

  if (day() < 10) {
    strcat(dateString, "0");
    dtostrf(day(), 1, 0, dateBuf);
  } else {
    dtostrf(day(), 2, 0, dateBuf);
  }
  strcat(dateString, dateBuf);
  strcat(dateString, "T");

  if (hour() < 10) {
    strcat(dateString, "0");
    dtostrf(hour(), 1, 0, dateBuf);
  } else {
    dtostrf(hour(), 2, 0, dateBuf);
  }

  strcat(dateString, dateBuf);
  strcat(dateString, ":");

  if (minute() < 10) {
    strcat(dateString, "0");
    dtostrf(minute(), 1, 0, dateBuf);
  } else {
    dtostrf(minute(), 2, 0, dateBuf);
  }

  strcat(dateString, dateBuf);
  strcat(dateString, ":");

  if (second() < 10) {
    strcat(dateString, "0");
    dtostrf(second(), 1, 0, dateBuf);
  } else {
    dtostrf(second(), 2, 0, dateBuf);
  }
  strcat(dateString, dateBuf);

  // timeZone
  strcat(dateString, "+01:00");

  Serial.println(dateString);
  return dateString;
}
