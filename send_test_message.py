import paho.mqtt.client as paho

broker="broker.hivemq.com"
port=1883

message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"c213182e462b51c147269b8c99b67545"}'

client= paho.Client()
client.connect(broker,port)
ret = client.publish("/mjson", message)
