# NanoSenAQM MQTT Client

This is an example implementation of an MQTT client application to upload
sensor data to the NanoSenAQM cloud system using an Arduino Uno. It uses a Wemos D1 board, which relies on the ESP8266 micro controller. The Wemos D1 is connected to a DHT22 temperature and humidity sensor module to collect test data.

The data upload mechanism consists of the following steps:

1. collect sensor data
1. build a JSON object with the sensor data
1. generate the signature of the message
1. build the message to upload which includes the JSON object with the sensor
   data and the signature of the JSON object
1. upload the message to the MQTT broker

**Note**: All Python scripts included in this project are made for Python 2.\*.

## Sensor data JSON object
Sensor data needs to be converted into a JSON object before being uploaded. This
JSON object allows to upload data from multiple sensors, and for each sensor
upload multiple values. The JSON object include the following properties:

| Key | Type | Description | Mandatory/Optional | Example |
|--|--|--|--|--|
| device_id | int | id of the device/cluster | Mandatory | 123456789 |
| message_date | string | date of the message (in ISO 8601 format)| Mandatory | 2019-07-10T13:46:59+00:00 |
| latitude | number | latitude of the device (in decimal degrees)| Optional |-38.5714 |
| longitude | number | longitude of the device (in decimal degrees)| Optional |-7.9135 |
| sensors | <sensor> array | array of sensor objects |Mandatory ||

### sensor object
The sensor object represents one sensor of the device. It is composed
of a sensor id and a list of data objects, where each data object represents a
data value. The sensor objects includes the following properties:

| Key | Type | Description | Mandatory/Optional |Example |
|--|--|--|--|--|
| sensor_id | int | id of the sensor | Mandatory |123456782 |
| data | <sensor> array | array of sensor data objects | Mandatory ||

### data object
The data object represents a data value of a sensor. This object includes the
sensor value and the acquisition date, as described bellow:

| Key | Type |Description | Mandatory/Optional | Example |
|--|--|--|--|--|
| value | number |sensor value to upload |  Mandatory | 54.20 |
| date | string | sensor value acquisition date (in ISO 8601 format)|  Mandatory  | 2019-07-10T13:46:59+00:00 |

### Example of a sensor data JSON object (prettified)

```json
{
  "device_id": 123456789,
  "message_date": "2019-07-10T13:46:59+00:00",
  "latitude": "-38.5714",
  "longitude": "-7.9135",
  "sensors": [
    {
      "sensor_id": 123456781,
      "data": [
        {
          "date": "2019-07-10T13:46:59+00:00",
          "value": "24.90"
        }
      ]
    }
  ]
}
```

## Signature
The security of the system is ensured by a mechanism based on Message Authentication Code (MAC), where the message is signed together with a secret key, which is only known by the server and the client. In the context of the NanoSenAQM project, to upload live sensor data, the signature is the MD5 sum of the secret key appended to the sensor data JSON object:

- signature = md5sum( sensor_data_JSON + secret_key )

where sensor_data_JSON is the sensor data JSON object represented as a string and secret_key is the secret key known only be the client
and the server.

## Upload message
The upload message is a JSON object represented as a string, which includes both the sensor data JSON object and its signature. The upload message JSON object should include the following properties:

| Key | Type | Description | Mandatory/Optional |Example |
|--|--|--|--|--|
| message | object | sensor data JSON object | Mandatory | {"device_id":..., "message_date":...,...} |
| authentication | string | signature of the data message | Mandatory |c213182e462b51c147269b8c99b67545|

**Note**: The sensor data JSON object representation in the upload message **should be exactly the same** as the one used to generate the signature, otherwise the signature verification method will fail and the data will be discarded.

### Example of upload message

The following example represents a RAW message uploaded by the client. The message includes both the data message object and the authentication signature:

```json
{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"c213182e462b51c147269b8c99b67545"}
```

To make it easier to read the RAW upload message presented above, bellow follows the same message after being formatted and prettified:

```json
{
  "message": {
    "device_id": 123456789,
    "message_date": "2019-07-10T13:46:59+00:00",
    "latitude": "-38.5714",
    "longitude": "-7.9135",
    "sensors": [
      {
        "sensor_id": 123456781,
        "data": [
          {
            "date": "2019-07-10T13:46:59+00:00",
            "value": "24.90"
          }
        ]
      }
    ]
  },
  "authentication": "c213182e462b51c147269b8c99b67545"
}
```


## Upload message to MQTT broker
The upload of the message is accomplished by the use of a MQTT client. This example uses the arduino-mqtt (https://github.com/256dpi/arduino-mqtt) library.

In the arduino-mqtt library, the default value for the maximum message size is 128 bytes. Since the size of the upload message is larger than 128 bytes, the library is initialized with a buffer size large enough to fit the entire upload message. If the message is larger than this value it will not be sent.

The initialization of the arduino-mqtt is made in the first lines of the sketch with the following instruction:

```c++
MQTTClient mqttClient(320);
```

# Testing the MQTT client
The MQTT client is currently configured to connect to the public MQTT broker `broker.hivemq.com` to allow for easy testing while the MQTT broker of the NanoSenAQM cloud system is still in tests and can get offline often.

As such, with the current configuration, the client uploads all data to the HiveMQ MQTT broker. This allows us to check if the data was properly received and if it respects the predefined message format.

To verify if the data is being properly uploaded, we included a Python script in the project that subscribes the topic "/mjson" in the HiveMQ MQTT broker and validates all messages received, validating both the format of the message and the authentication hash.

To do so, you just need to run the `test_message_received.py` python script:

```shell
user@host:~$ python test_message_received.py
```

Both the MQTT client and the `test_message_received.py` python script have the secret_key configured with the value `nanosen12345678`. If you modify this value, it should be modified in both scripts, otherwise the message will have authentication errors.

## Using other MQTT brokers
You can also use other MQTT brokers to test the client. To do so, you need to modify the MQTT broker address in both scripts.

A very widely used and easy to install MQTT broker is the Eclipse Mosquitto (https://mosquitto.org/). This can be easily installed in the most common Linux distributions, as well as in macOS and Windows.

# Test JSON messages
If you want to test if a JSON message is valid for testing or debugging purposes, you can use the `test_validate_message.py` Python script, which validates a JSON message according to the predefined specifications.

To use this script, you just need to modify it with the message that you want to test, the secret key used to sign message and invoke the validate function:

```python
secret_key = "nanosen12345678"

upload_message = '{"message":{"device_id":123456789,"message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":123456781,"data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.10"}]}]}, "authentication":"21995dadcd5aa1f0f9c6c70e1205ce1f"}'

validate_message(upload_message, secret_key)
```
