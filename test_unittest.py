import unittest
from validate_message import validate_message

class TestValidateMessage(unittest.TestCase):

    secret_key = "nanosen12345678"

    def test_valid_message_with_location(self):

        message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"c213182e462b51c147269b8c99b67545"}'

        expected = {"status":200, "message": ["message ok"]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)


    def test_valid_message_wihtout_location(self):
        message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"362459c376f2d72ca3fd494abbbb631b"}'

        expected = {"status":200, "message": ["message ok"]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)

        
    def test_message_with_missing_device_id(self):
        message = '{"message":{"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"d77475402e1e142d7e97c3d50bbcbc74"}'

        expected = {"status":400, "message": ["missing field: \"device_id\""]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)

    def test_message_only_with_longitude(self):
        message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"f0d51fdd3d07a471f60978171271d45c"}'

        expected = {"status":400, "message": ["missing field: \"latitude\""]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)

    
    def test_message_only_with_latitude(self):
        message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"d75aeb870215dc7050f64e640c575a03"}'
            
        expected = {"status":400, "message": ["missing field: \"longitude\""]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)

        
    def test_message_not_in_json_format(self):
        message = '"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"c213182e462b51c147269b8c99b67545"}'

        expected = {"status":400, "message": ["message not in json format"]}
        result = validate_message(message, self.secret_key)
       
        self.assertEquals(result, expected)


    def test_missing_authentication(self):
        message = '{"message-":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication":"c213182e462b51c147269b8c99b67545"}'

        expected = {"status":400, "message": ["missing field: \"message\""]}
        result = validate_message(message, self.secret_key)
        #print result
       
        self.assertEquals(result, expected)

        
    def test_missing_authentication(self):
        message = '{"message":{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}, "authentication-":"c213182e462b51c147269b8c99b67545"}'

        expected = {"status":400, "message": ["missing field: \"authentication\""]}
        result = validate_message(message, self.secret_key)
        #print result
       
        self.assertEquals(result, expected)

        
if __name__ == '__main__':
    unittest.main()
