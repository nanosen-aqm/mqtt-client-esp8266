import re
import json
import hashlib
from datetime import datetime

def validate_message(message, secret_key):

	responseList = []
	message_json = None
	message_data_json = None
	auth_message = None

	try:
                message = str(message)
		message_json = json.loads(message)
	except:
		response = {
			"status": 400,
			"message": ["message not in json format"]
		}
		print response
		return response	
	
	try:
		pass
	except expression as identifier:
		pass

	
	try:
		message_data_json = message_json["message"]
	except:
		print("An exception occurred, field \"message\" is missing")
		responseList.append("missing field: \"message\"")

	try:
		auth_message = message_json["authentication"]
	except:
		print("An exception occurred, field \"authentication\" is missing")
		responseList.append("missing field: \"authentication\"")

	if responseList:
		response = {
			"status": 400,
			"message": responseList
		}
		print response
		return response

	# Extract message field from original message keeping the format
	# to generate the MD5 sum
	regex = r".*?{.*?\"message\".*?:.*?({.*}).*,.*\"authentication\".*?:.*?\"(.*)\".*}"

	message_data_str = None
	if message_data_json:	
		try:
			matches = re.match(regex, message, re.MULTILINE)
			message_data_str = matches.group(1)
			print "Data message: " + str(message_data_str)
		except Exception as e:
			print e
			responseList.append("unable to parse received message")
			response = {
				"status": 400,
				"message": responseList
			}
			print response
			return response				
	else:
		response = {
				"status": 400,
				"message": responseList
		}
		print response
		return response

	# generate MD5 of message data
	dataMessageMD5 = hashlib.md5((message_data_str + secret_key).encode())
	dataMessageMD5Hash = dataMessageMD5.hexdigest()
	print "Data message MD5 hash shoud be: " + str(dataMessageMD5Hash)

	message_json =  message_json["message"]

	# compare the generated MD5 with the MD5 found in the message
	if dataMessageMD5Hash.lower() != auth_message.lower():
		print("An exception occurred, Authentication doesn't match")
		responseList.append("authentication failed")
	
	# check if all fields of the message are present with valid values
	try:
		field = int(message_json["device_id"])
	except:
		print("An exception occurred, no field called \"device_id\"")
		responseList.append("missing field: \"device_id\"")
	
	try:
		message_date = message_json["message_date"]
		if message_date:
			try:
				date = datetime.strptime(message_date[:19], "%Y-%m-%dT%H:%M:%S")
			except:
				print("An exception occurred: invalid format for \"message_date\"")
				responseList.append("invalid format: \"message_date\"")		
	except:
		print("An exception occurred, no field called \"message_date\"")
		responseList.append("missing field: \"message_date\"")
	
	if "latitude" in message_json:
		if "longitude" not in message_json:
			print("missing field: \"longitude\"")
			responseList.append("missing field: \"longitude\"")

		field = message_json["latitude"]
		try:
			field = float(field)
		except:
			print("An exception occurred, field \"latitude\" is not a float")
			responseList.append("field \"latitude\" should be a float")

	if "longitude" in message_json:
		if "latitude" not in message_json:
			print("missing field: \"latitude\"")
			responseList.append("missing field: \"latitude\"")

		field = message_json["longitude"]
		try:
			field = float(field)
		except:
			print("An exception occurred, field \"longitude\" is not a float")
			responseList.append("field \"longitude\" should be a float")

	try:
		message_json = message_json["sensors"]
	except:
		print("An exception occurred, no field called \"sensors\"")
		responseList.append("missing field: \"sensors\"")
	
	try:
		for sensor_data in message_json:
			try:
				field = int(sensor_data["sensor_id"])
			except:
				print("An exception occurred, no field called \"sensor_id\"")
				responseList.append("missing field:  \"sensor_id\"")
			try:
				c = sensor_data["data"]
				for data in c:
					try:
						date = data["date"]
						if date:
							try:
								date = datetime.strptime(date[:19], "%Y-%m-%dT%H:%M:%S")
							except:
								print("An exception occurred: invalid format for \"date\"")
								responseList.append("invalid format: \"date\"")		
					except:
						print("An exception occurred, no field called \"date\"")
						responseList.append("missing field: \"date\"")

					try:
						field = float(data["value"])
					except:
						print("An exception occurred, no field called \"value\"")
						responseList.append("missing field: \"value\"")

			except:
				print("An exception occurred, no field called \"data\"")
				responseList.append("missing field: \"data\"")

	except:
		print("An exception occurred, no field called \"sensors\"")
		responseList.append("missing field: \"sensors\"")


	response = None
	if not responseList:
		response = {
			"status": 200,
			"message": ["message ok"]
		}
		print response
		return response

	else:
		response = {
			"status": 400,
			"message": responseList
		}
		print response
		return response
